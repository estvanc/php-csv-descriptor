<?php
/**
 * Created by PhpStorm.
 * User: Lukáš Eštvanc
 * Date: 19.11.2019
 * Time: 11:27
 */

namespace Estvanc\PhpCsvDescriptor;


use Estvanc\PhpCsvDescriptor\Tests\Datatype;

class SchemaColumn
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     * Allowed datatypes are:
     *      INTEGER,
     *      STRING (default length 255),
     *      STRING
     *      NUMERIC
     *      DATE
     */
    protected $dataType;

    /**
     * @var bool
     */
    protected $nullable;

    /**
     * @var string
     *      DATE (default format YYYY-mm-dd),
     *      DATE[format] -> https://www.php.net/manual/en/function.date.php
     */
    protected $format;

    /**
     * @var integer
     */
    protected $maxLength;

    /**
     * @var integer
     */
    protected $maxDecimals;


    public function getName() {return $this->name;}
    public function getDataType() {return $this->dataType;}
    public function getFormat() {return $this->format;}
    public function getNullable() {return $this->nullable;}
    public function getMaxLength() {return $this->maxLength;}
    public function getMaxDecimals() {return $this->maxDecimals;}

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $dataType
     * @throws \Exception
     */
    public function setDataType($dataType)
    {
        if ($this->name === null)
            throw new \Exception("Name of the coulumn must be set prior to dataType");



        if ($dataType != Datatype::UNKNOWN && array_search($dataType, Datatype::DATATYPES) === false)
            throw new \Exception("Unknown datatype for column {$this->name}: $dataType");
        $this->dataType = $dataType;
    }

    /**
     * @param $format
     * @throws \Exception
     */
    public function setFormat($format)
    {
        if ($this->name === null)
            throw new \Exception("Name of the coulumn must be set prior to format");

        if ($this->dataType === null)
            throw new \Exception("Datatype of the coulumn {$this->name} must be set prior to format");

        if ($this->dataType != Datatype::DATE && $this->dataType != Datatype::DATETIME)
            throw new \Exception("Format can be set only for datatypes ".Datatype::DATE." and ".Datatype::DATETIME.
                "You've tried to set it to column {$this->name} for datatype {$this->dataType}");

        $this->format = $format;
    }


    /**
     * @param $nullable
     * @throws \Exception
     */
    public function setNullable($nullable)
    {
        if ($this->name === null)
            throw new \Exception("Name of the coulumn must be set prior to nullable");

        if ($this->dataType === null)
            throw new \Exception("Datatype of the coulumn {$this->name} must be set prior to nullable");

        $this->nullable = $nullable;
    }

    /**
     * @param $maxLength
     * @throws \Exception
     */
    public function setMaxLength($maxLength)
    {
        if ($this->name === null)
            throw new \Exception("Name of the coulumn must be set prior to maxLength");

        if ($this->dataType === null)
            throw new \Exception("Datatype of the coulumn {$this->name} must be set prior to maxLength");

        $this->maxLength = $maxLength;
    }


    /**
     * @param $maxDecimals
     * @throws \Exception
     */
    public function setMaxDecimals($maxDecimals)
    {
        if ($this->name === null)
            throw new \Exception("Name of the coulumn must be set prior to maxDecimals");

        if ($this->dataType === null)
            throw new \Exception("Datatype of the coulumn {$this->name} must be set prior to maxDecimals");
        $this->maxDecimals = $maxDecimals;
    }

    public function addColumnFromYamlArray($yamlArray)
    {
        $this->setName($yamlArray['Name']);
        $this->setDataType($yamlArray['Datatype']);
        $this->setNullable($yamlArray['Nullable']);

        if (isset($yamlArray['Format']))
            $this->setFormat($yamlArray['Format']);

        if (isset($yamlArray['MaxLength']))
            $this->setMaxLength($yamlArray['MaxLength']);

        if (isset($yamlArray['MaxDecimals']))
            $this->setMaxDecimals($yamlArray['MaxDecimals']);

    }

    public function getColumnAsYamlArray()
    {
        $yamlArray = [];
        $yamlArray['Name'] = $this->getName();
        $yamlArray['Datatype'] = $this->getDataType();
        $yamlArray['Nullable'] = $this->getNullable();
        $yamlArray['Format'] = $this->getFormat();
        $yamlArray['MaxLength'] = $this->getMaxLength();
        $yamlArray['MaxDecimals'] = $this->getMaxDecimals();
        return $yamlArray;
    }

}