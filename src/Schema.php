<?php
/**
 * Created by PhpStorm.
 * User: Lukáš Eštvanc
 * Date: 19.11.2019
 * Time: 11:22
 */

namespace Estvanc\PhpCsvDescriptor;


use Estvanc\PhpCsvDescriptor\Tests\ColumnSeparator;
use Estvanc\PhpCsvDescriptor\Tests\EscapeChar;
use Estvanc\PhpCsvDescriptor\Tests\Header;
use Estvanc\PhpCsvDescriptor\Tests\LineSeparator;
use Estvanc\PhpCsvDescriptor\Tests\Quotes;
use Symfony\Component\Yaml\Yaml;


class Schema
{
    /**
     * @var string
     */
    protected $lineSeparator = LineSeparator::AUTO;

    /**
     * @var string
     */
    protected $columnSeparator = ColumnSeparator::AUTO;

    /**
     * @var string
     */
    protected $quotes = Quotes::AUTO;

    /**
     * @var string
     */
    protected $escapeChar = EscapeChar::AUTO;

    protected $header = Header::AUTO;

    protected $gzipped = false;

    /**
     * @var SchemaColumn[]
     */
    protected $columns = [];

    public function getLineSeparator(){return $this->lineSeparator;}
    public function getColumnSeparator(){return $this->columnSeparator;}
    public function getQuotes(){return $this->quotes;}
    public function getEscapeChar(){return $this->escapeChar;}
    public function getHeader(){return $this->header;}
    public function getGzipped(){return $this->gzipped;}
    public function getColumns(){return $this->columns;}

    public function getColumnsAsYamlArray()
    {
        $yamlArray = [];
        foreach ($this->columns AS $column)
        {
            $yamlArray[] = $column->getColumnAsYamlArray();
        }
        return $yamlArray;
    }


    public function setLineSeparator($value){$this->lineSeparator = $value;}
    public function setColumnSeparator($value){$this->columnSeparator = $value;}
    public function setQuotes($value){$this->quotes = $value;}
    public function setEscapeChar($value){$this->escapeChar = $value;}
    public function setHeader($value){$this->header = $value;}
    public function setGzipped($value){$this->gzipped = $value;}
    public function setColumns($value){$this->columns = $value;}

    public function setColumnsAsYamlArray($yamlArray)
    {
        foreach ($yamlArray AS $columnArray)
        {
            $column = new SchemaColumn();
            $column->addColumnFromYamlArray($columnArray);
        }
    }


    public function __construct()
    {
    }

    /**
     * @param SchemaColumn $column
     * @throws \Exception
     */
    public function addColumn($column)
    {
        foreach ($this->columns AS $currentColumn)
        {
            if ($column->getName() === $currentColumn->getName()) throw new \Exception("Column name ".$column->getName()." already exists in schema");
        }
        $this->columns[] = $column;
    }

    public function getSchemaFromYaml($yamlString)
    {
        $yamlArray =  Yaml::parse($yamlString);
        $this->setLineSeparator($yamlArray['LineSeparator']);
        $this->setColumnSeparator($yamlArray['ColumnSeparator']);
        $this->setQuotes($yamlArray['Quotes']);
        $this->setEscapeChar($yamlArray['EscapeChar']);
        $this->setHeader($yamlArray['Header']);
        $this->setGzipped($yamlArray['Gzipped']);
        $this->setColumnsAsYamlArray($yamlArray['Columns']);
    }

    public function getYamlFromSchema()
    {
        $yamlArray = [];
        $yamlArray['LineSeparator'] = $this->getLineSeparator();
        $yamlArray['ColumnSeparator'] = $this->getColumnSeparator();
        $yamlArray['Quotes'] = $this->getQuotes();
        $yamlArray['EscapeChar'] = $this->getEscapeChar();
        $yamlArray['Header'] = $this->getHeader();
        $yamlArray['Gzipped'] = $this->getGzipped();
        $yamlArray['Columns'] = $this->getColumnsAsYamlArray();
        return Yaml::dump($yamlArray, 2, 4);

    }







}