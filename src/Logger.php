<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 22.09.2019
 * Time: 11:55
 */

namespace Estvanc\PhpCsvDescriptor;

class Logger
{
    /**
     * @var \Monolog\Logger
     */
    public $logger;


    /**
     * Logger constructor.
     * @param $name
     * @throws \Exception
     * @returns \Monolog\Logger
     */
    public function __construct($name)
    {
        $this->logger = new \Monolog\Logger($name);
        $dir = dirname(__FILE__);
        $this->logger->pushHandler(new \Monolog\Handler\StreamHandler($dir.'/../app.log', \Monolog\Logger::INFO));
        $this->logger->pushHandler(new \Monolog\Handler\StreamHandler($dir.'/../app.err', \Monolog\Logger::WARNING));
        $this->logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::DEBUG));
        $this->logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stderr', \Monolog\Logger::WARNING));
        return $this;
    }

    /**
     * Logger getter
     * @returns \Monolog\Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

}
