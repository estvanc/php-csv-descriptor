<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 19:10
 */

namespace Estvanc\PhpCsvDescriptor;
use Estvanc\PhpCsvDescriptor\Tests\ColumnSeparator;
use Estvanc\PhpCsvDescriptor\Tests\Datatype;
use Estvanc\PhpCsvDescriptor\Tests\Header;
use Estvanc\PhpCsvDescriptor\Tests\Quotes;
use Estvanc\PhpCsvDescriptor\Tests\Columns;
use Estvanc\PhpCsvDescriptor\Tests\ColumnsAttributes;

use Estvanc\PhpCsvDescriptor\Tests\EscapeChar;
use Symfony\Component\Yaml\Yaml;
use Estvanc\PhpCsvDescriptor\Tests\LineSeparator;

use SplFileInfo;
use Estvanc\PhpCsvDescriptor\Schema;



class CsvSchema
{
    /**
     * @var SplFileInfo
     */
    protected $csvFile;

    /**
     * @var SplFileInfo
     */
    protected $schemaFile;



    protected $gzipped = false;

    protected $logger;


    /**
     * @var mixed Schema
     */
    protected $schema;


    protected function getSchemaFileForCsvFile(SplFileInfo $csvFile)
    {
        $csvFilename = $csvFile->getFilename();
        $csvFilenameExtension = $csvFile->getExtension();
        if ($csvFilenameExtension == 'gz')
        {
            $this->gzipped = true;
            $schemaFilename = substr($csvFilename, 0, -3).'.schema.yaml';
        }
        else {
            $schemaFilename = $csvFilename.'.schema.yaml';
        }
        return realpath($csvFile->getPath()."/".$schemaFilename);


    }




    /**
     * CsvSchema constructor.
     * @param string $file
     * @param \Monolog\Logger $logger
     */
    public function __construct($file, $logger)
    {
        $this->logger = $logger;
        $this->csvFile =  new SplFileInfo(realpath($file));
        $schemaFullPath = $this->getSchemaFileForCsvFile($this->csvFile);
        $this->schema = new Schema();
        if ($this->gzipped) $this->schema->setGzipped(true);

        if (file_exists($schemaFullPath))
        {

            $this->schema->loadFromFile($this->schemaFile);
            $logger->debug("Schema loaded from file ".$schemaFullPath);
        }
    }

    public function loadSchemaFromString($string)
    {
        $this->schema =  Yaml::parse($string);
    }

    public function detectAllAutoValues($forceAll = false, $maxLineCount = 1000)
    {
        if ($forceAll || $this->schema->getLineSeparator() == LineSeparator::AUTO)
            $this->schema->setLineSeparator(LineSeparator::getLineSeparator($this->csvFile, $this->logger));

        if ($forceAll || $this->schema->getColumnSeparator() == ColumnSeparator::AUTO)
            $this->schema->setColumnSeparator(ColumnSeparator::getColumnSeparator($this->csvFile, $this->logger));

        if ($forceAll || $this->schema->getQuotes() == Quotes::AUTO)
            $this->schema->setQuotes(Quotes::getQuotes($this->csvFile, $this->logger));

        if ($forceAll || $this->schema->getEscapeChar() == EscapeChar::AUTO)
            $this->schema->setEscapeChar(\Estvanc\PhpCsvDescriptor\Tests\EscapeChar::getEscapeChar($this->csvFile, $this->schema->getQuotes(), $this->logger));

        if ($forceAll || $this->schema->getHeader() == Header::AUTO)
            $this->schema->setHeader(\Estvanc\PhpCsvDescriptor\Tests\Header::getHeader($this->csvFile, $this->schema->getColumnSeparator(), $this->logger));

        if ($forceAll || count($this->schema->getColumns()) == 0)
            $this->setColumns(Columns::getColumns($this->csvFile, $this->schema->getHeader(), $this->schema->getColumnSeparator(), $this->schema->getLineSeparator(), $this->logger));

        $hasUnknown = false;
        foreach ($this->schema->getColumns() AS $column)
        {
            if ($column->getDataType() == Datatype::UNKNOWN) $hasUnknown = true;
        }
        if ($forceAll || $hasUnknown)
            $this->schema->setColumns(ColumnsAttributes::getColumnsAttributes($this->csvFile, $this->schema->getHeader(), $this->schema->getColumnSeparator(), $this->schema->getColumns(), $this->logger, $maxLineCount));
    }





    public function setColumns($columns)
    {
        $this->removeAllColumns();
        foreach ($columns AS $column)
        {
            $newColumn = [];
            $newColumn['name'] = $column['name'];
            $newColumn['datatype'] = $column['datatype'];
            $newColumn['nullable'] = $column['nullable'];
            if ($column['datatype'] == Datatype::DATE || $column['datatype'] == Datatype::DATETIME)
            {
                $newColumn['format'] = $column['format'];
            }
            $newColumn['maxLength'] = $column['maxLength'];
            if ($column['datatype'] == Datatype::NUMERIC)
            {
                $newColumn['maxDecimals'] = $column['maxDecimals'];
            }
            $this->schema['Columns'][] = $newColumn;
        }
    }

    public function removeAllColumns()
    {
        $this->schema['Columns'] = [];
        return $this;
    }

    public function addColumn($name, $dataType, $nullable, $format)
    {
        $column['name'] = $name;
        $column['datatype'] = $dataType;
        $column['nullable'] = $nullable;

        if ($column['datatype'] == Datatype::DATE) $column['format'] = $format;
        $this->schema['Columns'][] = $column;
        return $this;
    }

    public function getColumns(){return $this->schema['Columns'];}

    public function printSchema()
    {
        echo("Schema for file: ".$this->file->filename."\n");
        $yamlSchema = $this->schema->getYamlFromSchema();
        echo ($yamlSchema."\n");
        echo("For edit schema: vim ".$this->file->schemaFullPath);

    }



    public function publishSchema($fullFilePath = null)
    {
        if ($fullFilePath === null)
        {
            $fullFilePath = $this->getSchemaFileForCsvFile($this->csvFile);
        }

        $yamlSchema = $this->schema->getYamlFromSchema();
        file_put_contents($fullFilePath, $yamlSchema);
    }
}