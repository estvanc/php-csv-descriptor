<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 19:59
 */

namespace Estvanc\PhpCsvDescriptor\Tests;


class ColumnSeparator
{
    const maxLineNumberCountForAnalyse = 500;

    const AUTO = "AUTO";
    const UNKNOWN = "UNKNOWN";
    const TAB = "\\t";
    const PIPE = "|";

    //default, also for files with one column
    const COMMA = ",";
    const COLON = ":";
    const SEMICOLON = ";";



    const SEPARATORS = array(
        self::COMMA => ",",
        self::TAB => "\t",
        self::PIPE => "|",
        self::COLON => ":",
        self::SEMICOLON => ";"
        );

    /**
     * @param \SplFileInfo $file
     * @param \Monolog\Logger $logger
     */
    public static function getColumnSeparator(\SplFileInfo $file, $logger)
    {
        $logger->debug("ColumnSeparator detection START");
        $handle = fopen($file->getRealPath(), "r");

        $results = [];
        $lineNumber = 0;

        $resultSeparator = self::COMMA;

        while (($line = fgets($handle)) && $lineNumber < self::maxLineNumberCountForAnalyse)
        {
            $lineNumber++;
            foreach(self::SEPARATORS AS $separatorName => $separator)
            {

                $array = explode($separator, $line);
                $arrayCount = count($array);
                if (isset($results[$separatorName][$arrayCount]) == false) $results[$separatorName][$arrayCount] = 0;
                $results[$separatorName][$arrayCount]++;
            }
        }
        fclose($handle);

        $separatorFound = false;

        for ($i=$lineNumber; $separatorFound == false && $i > 1; $i--)
        {
            $possibleSeparators = [];
            foreach ($results AS $separator => $separatorCountsArray)
            {
                foreach ($separatorCountsArray AS $fields => $count)
                {
                    if ($fields > 1 && $count == $i)
                    {
                        $possibleSeparators[$separator] = $fields;
                        $separatorFound = true;
                    }
                }
            }
            if ($separatorFound == true)
                $maxFieldCound = -1;
                foreach ($possibleSeparators AS $separator=>$fieldCount)
                {
                    if ($fieldCount > $maxFieldCound)
                    {
                        $maxFieldCound = $fieldCount;
                        $resultSeparator = $separator;
                    }
                }
        }

        $logger->debug("ColumnSeparator detection FINISHED. Found: $resultSeparator");

        return $resultSeparator;
        
    }




}