<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 22:34
 */

namespace Estvanc\PhpCsvDescriptor\Tests;
use Monolog\Logger;


class EscapeChar
{
    const AUTO = 'AUTO';
    const NONE = 'NONE';
    const DOUBLEQUOTES = 'DOUBLE';
    const SINGLEQUOTES = "SINGLE";
    const BACKSLASH = 'BACKSLASH';

    const ESCAPECAHES = array(
        self::DOUBLEQUOTES => '"',
        self::SINGLEQUOTES => "'",
        self::BACKSLASH => "\\"

    );

    /**
     * @param \SplFileInfo
     * @param string $quotes
     * @param Logger $logger
     * @return int|string
     */
    public static function getEscapeChar(\SplFileInfo $file,  $quotes, $logger)
    {
        $content = file_get_contents($file->getRealPath(), false, null, 0, 1000000);
        $result = self::NONE;
        if ($quotes == Quotes::DOUBLEQUOTES)
        {
            $doubleQuoteCount = substr_count($content, self::ESCAPECAHES[self::DOUBLEQUOTES].self::ESCAPECAHES[self::DOUBLEQUOTES]);
            $backslashCount = substr_count($content, self::ESCAPECAHES[self::BACKSLASH].self::ESCAPECAHES[self::DOUBLEQUOTES]);
            if ($backslashCount > $doubleQuoteCount) $result =  self::BACKSLASH;
            else $result = self::DOUBLEQUOTES;
        }
        if ($quotes == Quotes::SINGLEQUOTES)
        {
            $doubleQuoteCount = substr_count($content, self::ESCAPECAHES[self::SINGLEQUOTES].self::ESCAPECAHES[self::SINGLEQUOTES]);
            $backslashCount = substr_count($content, self::ESCAPECAHES[self::BACKSLASH].self::ESCAPECAHES[self::SINGLEQUOTES]);
            if ($backslashCount > $doubleQuoteCount) $result = self::BACKSLASH;
            else $result = self::SINGLEQUOTES;
        }
        if ($result == self::NONE) $result = self::BACKSLASH;

        return $result;
    }
}