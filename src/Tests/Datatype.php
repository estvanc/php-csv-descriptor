<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 23:27
 */

namespace Estvanc\PhpCsvDescriptor\Tests;


class Datatype
{
    const UNKNOWN="UNKNOWN";
    const DATE="DATE";
    const DATETIME="DATETIME";
    const INTEGER="INTEGER";
    const NUMERIC="NUMERIC";
    const STRING="STRING";

    const DATATYPES= [self::DATE, self::DATETIME, self::INTEGER, self::NUMERIC, self::STRING];


    const DATEFORMATS =
        [
            'Ymd','Y-m-d','d.m.Y','d/m/Y','m.d.Y','m/d/Y',
            'ymd','y-m-d','d.m.y','d/m/y','m.d.y','m/d/y',

        ];

    const DATETIMEFORMATS =
        [
            'Ymd H:i:s','Y-m-d H:i:s','d.m.Y H:i:s','d/m/Y H:i:s','m.d.Y H:i:s','m/d/Y H:i:s',
            'ymd H:i:s','y-m-d H:i:s','d.m.y H:i:s','d/m/y H:i:s','m.d.y H:i:s','m/d/y H:i:s',

        ];

}