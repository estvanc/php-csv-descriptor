<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 19:59
 */

namespace Estvanc\PhpCsvDescriptor\Tests;
use Monolog\Logger;


class LineSeparator
{
    const AUTO = "AUTO";
    const UNKNOWN = "UNKNOWN";
    const LF = "\\n";
    const CRLF = "\\r\\n";



    const SEPARATORS = array(
        self::LF => "\n",

        // MUST BE SECOND - AFTER LF
        self::CRLF => "\r\n",
        );

    public static function getLineSeparator(\SplFileInfo $file, Logger $logger)
    {
        var_export($file->getRealPath());
        $logger->debug("LineSeparator detection START");
        $content = file_get_contents($file->getRealPath(), false, null, 0, 1000000);
        $cur_eol = self::UNKNOWN;
        $cur_eol_count = 0;
        foreach(self::SEPARATORS as $name => $separator){
            $count = substr_count($content, $separator);
            if( $count >= $cur_eol_count)
            {
                $cur_eol = $name;
                $cur_eol_count = $count;

            }
        }
        $logger->debug("LineSeparator detection FINISHED. Found: $cur_eol");
        return $cur_eol;
    }
}