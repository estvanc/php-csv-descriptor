<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 22:24
 */

namespace Estvanc\PhpCsvDescriptor\Tests;
use Monolog\Logger;


class Quotes
{
    const NONE = 'NONE';
    const AUTO = 'AUTO';
    const DOUBLEQUOTES = 'DOUBLE';
    const SINGLEQUOTES = "SINGLE";




    const QUOTES = array(
        self::DOUBLEQUOTES => '"',
        self::SINGLEQUOTES => "'",

    );

    public static function getQuotes(\SplFileInfo $file, Logger $logger)
    {
        $content = file_get_contents($file->getRealPath(), false, null, 0, 1000000);
        $cur_q = self::DOUBLEQUOTES;
        $cur_q_count = -1;
        foreach(self::QUOTES as $name => $q){
            $count = substr_count($content, $q);
            if( $count > $cur_q_count)
            {
                $cur_q = $name;
                $cur_q_count = $count;

            }
        }
        return $cur_q;
    }
}