<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 23:01
 */

namespace Estvanc\PhpCsvDescriptor\Tests;
use Monolog\Logger;

class Header
{
    const AUTO = "AUTO";
    const TRUE = 'YES';
    const FALSE = 'NO';

    /**
     * @param \SplFileInfo $file
     * @param string $quotes
     * @param Logger $logger
     * @return int|string
     */
    public static function getHeader(\SplFileInfo $file,  $columnSeparator, $logger)
    {
        $logger->debug("Header detection START");
        $handle = fopen($file->getRealPath(), "r");

        $lineArray = fgetcsv($handle, 0, ColumnSeparator::SEPARATORS[$columnSeparator]);
        $result = self::TRUE;
        foreach ($lineArray AS $column)
        {
            if (is_numeric($column)) $result = self::FALSE;
            if ($column == '') $result = self::FALSE;
        }
        $logger->debug("Header detection FINISHED. Header is present: $result");
        return $result;


    }

}