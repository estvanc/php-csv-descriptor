<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 23:01
 */

namespace Estvanc\PhpCsvDescriptor\Tests;
use Estvanc\PhpCsvDescriptor\SchemaColumn;
use Estvanc\PhpCsvDescriptor\Tests\Header;
use Estvanc\PhpCsvDescriptor\Tests\Datatype;
use Estvanc\PhpCsvDescriptor\Tests\LineSeparator;
use Monolog\Logger;

class Columns
{

    /**
     * @param \SplFileInfo $file
     * @param string $quotes
     * @param Logger $logger
     * @return SchemaColumn[]
     */
    public static function getColumns(\SplFileInfo $file,  $header, $columnSeparator, $lineSeparator, $logger)
    {
        $logger->debug("ColumnNames detection START");
        //var_export($file);
        $handle = fopen($file->getRealPath(), "r");

        $lineArray = fgetcsv($handle, 0, ColumnSeparator::SEPARATORS[$columnSeparator]);

        $resultColumns = [];
        if ($header == Header::FALSE)
        {

            for ($i=1; $i <= count($lineArray); $i++)
            {
                $column = new SchemaColumn();
                $column->setName("column_".$i);
                $column->setDatatype(Datatype::UNKNOWN);
                $column->setNullable(true);
                $column->setFormat("");
                $column->setMaxLength(0);
                $column->setMaxDecimals(0);

                $resultColumns[] = $column;
            }
        }
        else
        {
            foreach ($lineArray AS $columnName)
            {
                $column = new SchemaColumn();
                $column->setName($columnName);
                $column->setDatatype(Datatype::UNKNOWN);
                $column->setNullable(true);
                $column->setFormat("");
                $column->setMaxLength(0);
                $column->setMaxDecimals(0);

                $resultColumns[] = $column;
            }
        }
        $logger->debug("ColumnNames detection FINISHED. Names:".self::getColumnNamesAsString($resultColumns));

        return $resultColumns;
    }

    public static function getColumnNamesAsString($resultColumns)
    {
        $stringColumns = [];
        foreach ($resultColumns AS $column)
        {
            $stringColumns[] = $column['name'];
        }
        return implode(',', $stringColumns);

    }

}