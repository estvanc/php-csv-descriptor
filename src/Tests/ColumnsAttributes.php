<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 23:01
 */

namespace Estvanc\PhpCsvDescriptor\Tests;
use Estvanc\PhpCsvDescriptor\Tests\Header;
use Estvanc\PhpCsvDescriptor\Tests\Datatype;
use Estvanc\PhpCsvDescriptor\Tests\LineSeparator;


use Monolog\Logger;

class ColumnsAttributes
{


    /**
     * @param \SplFileInfo $file
     * @param string $quotes
     * @param Logger $logger
     * @return int|string
     */
    public static function getColumnsAttributes(\SplFileInfo $file, $header, $columnSeparator, $columns, $logger, $maxLineCount = 1000)
    {
        $logger->debug("ColumnNames detection START. Maximum $maxLineCount lines.");
        //var_export($file);
        $handle = fopen($file->getRealPath(), "r");

        $columnSeparatorChar = ColumnSeparator::SEPARATORS[$columnSeparator];


        $resultColumns = [];
        if ($header == Header::TRUE)
        {
            fgetcsv($handle, 0, $columnSeparatorChar);
        }

        $lineCount =0;


        $resultColumns = $columns;

        foreach ($resultColumns AS $key=>$columnIndex)
        {

            $resultColumns[$key]['maxLength'] = 0;
            $resultColumns[$key]['maxDecimals'] = 0;
            $resultColumns[$key]['dateFormats'] = Datatype::DATEFORMATS;
            $resultColumns[$key]['datetimeFormats'] = Datatype::DATETIMEFORMATS;
            $resultColumns[$key]['datatype'] = Datatype::UNKNOWN;
        }


        while (($lineArray = fgetcsv($handle, 0,$columnSeparatorChar)) && $lineCount < $maxLineCount)
        {
            //if ($lineCount % 1000000 == 0) $logger->debug("ColumnsAttributes: Processed $lineCount rows");
            $lineCount++;

            if (count($lineArray) != count($resultColumns)) continue;

            foreach ($lineArray AS $columnIndex => $value)
            {
                $cdt = $resultColumns[$columnIndex]['datatype'];
                //echo ($value);
                //if (self::isInteger($value)) echo " INT ";
                //echo ",";
                if (($cdt == Datatype::UNKNOWN || $cdt == Datatype::DATETIME ) && self::isDateOrDatetime($value, $resultColumns[$columnIndex], 'datetimeFormats', Datatype::DATETIME))
                {
                    //this can be empty, values are set directly in self::isDateOrDatetime

                }

                else if (($cdt == Datatype::UNKNOWN || $cdt == Datatype::DATETIME || $cdt == Datatype::DATE  ) && self::isDateOrDatetime($value, $resultColumns[$columnIndex], 'dateFormats', Datatype::DATE))
                {

                    //this can be empty, values are set directly in self::isDateOrDatetime
                }

                else if (($cdt == Datatype::UNKNOWN || $cdt == Datatype::DATETIME || $cdt == Datatype::DATE || $cdt == Datatype::INTEGER) && (self::isInteger($value)))
                {
                   // echo ("INT\r\n");
                    $resultColumns[$columnIndex]['datatype'] = Datatype::INTEGER;
                }
                else if (($cdt == Datatype::UNKNOWN || $cdt == Datatype::DATETIME || $cdt == Datatype::DATE || $cdt == Datatype::INTEGER || $cdt == Datatype::NUMERIC) && is_numeric($value))
                {
                    //echo ("NUMERIC\r\n");
                    if ($columnIndex == 2) echo($value."\r\n");
                    $resultColumns[$columnIndex]['datatype'] = Datatype::NUMERIC;
                    $resultColumns[$columnIndex]['maxDecimals'] = max($resultColumns[$columnIndex]['maxDecimals'], self::numberOfDecimals($value));
                }
                else
                {
                   // echo ("STRING\r\n");
                    $resultColumns[$columnIndex]['datatype'] = Datatype::STRING;
                }
                $resultColumns[$columnIndex]['maxLength'] = max($resultColumns[$columnIndex]['maxLength'], strlen($value));

            }

        }
        foreach ($resultColumns AS $column)
        {
            $logger->debug("Column {$column['name']} has datatype {$column['datatype']}");
        }

        $logger->debug("ColumnNames detection FINISHED");


        return $resultColumns;


    }


    public static function validateDate($date, $format)
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function numberOfDecimals($value)
    {
        if ((int)$value == $value)
        {
            return 0;
        }
        else if (! is_numeric($value))
        {
            // throw new Exception('numberOfDecimals: ' . $value . ' is not a number!');
            return false;
        }

        return strlen($value) - strrpos($value, '.') - 1;
    }

    public static function isInteger($input){
        $x = strval($input);
        if (strlen($x) > 0 && substr($x, 0, 1) == '-') $x = substr($x, 1);
        return(ctype_digit($x));
    }

    public static function isDateOrDatetime($value, &$resultColumn, $formatArrayKey, $datatype)
    {

        if ($resultColumn[$formatArrayKey] == null) return false;

        $newDateFormatArray = [];
        $dateFormatArray = $resultColumn[$formatArrayKey];
        foreach ($dateFormatArray AS $dateFormat)
        {
            if (self::validateDate($value, $dateFormat))
            {
                $newDateFormatArray[] = $dateFormat;
            }
        }
        if (count($newDateFormatArray) == 0) $resultColumn[$formatArrayKey] = null;
        else
        {
            $resultColumn[$formatArrayKey] = $newDateFormatArray;
            $resultColumn['format'] = $newDateFormatArray[0];
            $resultColumn['datatype'] = $datatype;
            $resultColumn['maxLength'] = max($resultColumn['maxLength'], strlen($value));
            return true;
        }
        return false;

    }



}