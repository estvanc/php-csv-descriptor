<?php
/**
 * Created by PhpStorm.
 * User: kaso
 * Date: 27.10.2019
 * Time: 19:13
 */

$loader = require __DIR__ . '/vendor/autoload.php';

use Estvanc\PhpCsvDescriptor\CsvSchema;
use Estvanc\PhpCsvDescriptor\Tests\LineSeparator;
use Estvanc\PhpCsvDescriptor\Tests\Columns;
use Estvanc\PhpCsvDescriptor\Tests\ColumnsAttributes;

use Estvanc\PhpCsvDescriptor\Tests\ColumnSeparator;
use Estvanc\PhpCsvDescriptor\Tests\Quotes;
use Estvanc\PhpCsvDescriptor\Logger;
use Symfony\Component\Yaml\Yaml;

/*$schema = new CsvSchema();
$schema->setLineSeparator('AUTO')
    ->addColumn('aa', 'bbba')
    ->addColumn('bb', 'asdasd');
$schema->saveToYaml();*/

//echo ColumnsAttributes::validateDate('2019-01-01', 'Y-m-d');

//echo realpath('data/test.csv');



$file = 'data/test.csv';
//var_export(realpath($file->getRealPath()));

$lC = new Logger('CsvTest');
$l = $lC->getLogger();

$csvSchema = new CsvSchema($file, $l);

$csvSchema->detectAllAutoValues(false, 1000);
$csvSchema->printSchema();
//$csvSchema->publishSchema();




